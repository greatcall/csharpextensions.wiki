# CSharpExtensions
Contains extension methods for C# types

### How do I install it?
`install-package GreatCall.CSharpExtensions`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/csharpextensions.wiki/wiki/).